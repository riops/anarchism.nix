{ pkgs, stdenv, fetchgit, makeDesktopItem }:

stdenv.mkDerivation rec {
  name = "anarchism-${version}";
  version = "15.3";

  src = fetchgit {
    url = https://0xacab.org/ju/afaq.git;
    sha256 = "1dahx1ja7vhdms2pddgcilw85m8wzvhkz7sm15951lwapawxfq64";
    rev = "refs/tags/${version}";
  };

  installPhase = ''
    mkdir -p $out/share/doc/anarchism
    mv html markdown $out/share/doc/anarchism/
    mkdir -p $out/bin
    echo ${pkgs.elinks}/bin/elinks $out/share/doc/anarchism/html/index.html > $out/bin/anarchism
    chmod a+x $out/bin/anarchism
  '';

  desktopItem = makeDesktopItem {
    name = "AFAQ";
    exec = "xdg-open file://$out/share/doc/anarchism/html/index.html";
    comment = meta.description;
    desktopName = "Anarchism";
    genericName = "Anarchism";
    categories = "Education;";
  };

  meta = with stdenv.lib; {
    description = "Exhaustive exploration of Anarchist theory and practice.";
    longDescription = ''
      The Anarchist FAQ is an excellent source of information regarding
      Anarchist (libertarian socialist) theory and practice. It covers all major
      topics, from the basics of Anarchism to very specific discussions of
      politics, social organization, and economics.
    '';
    homepage = http://www.anarchistfaq.org;
    license = stdenv.lib.licenses.gpl2;
    maintainers = [{
      name = "riops";
      email = "riops@riseup.net";
      keys = [{
        longkeyid = "rsa4096/0x4AF608DB583B6321";
        fingerprint = "E99E E608 5AEA BE17 6012  BA7D 4AF6 08DB 583B 6321";
      }];
    }];
    platforms = platforms.linux;
  };
}
