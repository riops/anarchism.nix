anarchism.nix
====

**"An Anarchist F.A.Q." (AFAQ)**
----

NixOS port of the popular "anarchism" debian package, using elinks as frontend.

Declarative install:
----

Add to your system derivation by putting this into your ``/etc/nixos/configuration.nix``:
```nix
let
  anarchism = fetchTarball https://0xacab.org/riops/anarchism.nix/-/archive/master/anarchism.nix.tar.gz;
in {
  environment.systemPackages = with pkgs; [ anarchism ];
}
```

License
--------
GPLv2

Copyright
----------
1995-2020 The Anarchist FAQ Editorial Collective <anarchistfaq at yahoo dot co dot uk>
